#include "bmp/bmpio.h"
#include "file/file.h"
#include "rotate/rotate.h"
#include "util/bmp_error.h"
#include "util/file_error.h"
#include "util/transform_error.h"



static int open_files(const char *ch_input, const char *ch_output, FILE **input, FILE **output);


int main(int argc, char** argv) {
    if (argc < 3) {
        return argc;
    }
    // opening files
    FILE *input, *output;
    if (open_files(argv[1], argv[2], &input, &output)) { 
        file_print_status(FILE_ERROR);
        return 1;
    }
    // converting file from bmp
    struct image img;
    enum read_status r_status = from_bmp(input, &img);
    if (r_status) {
        bmp_print_read_status(r_status);
        goto destroy;
        return r_status;
    }
    // rotating file
    struct image_optional image_rotated = rotate(img);
    if (!image_rotated.is_valid) {
        transform_print_error();
        goto destroy;
        return 1;
    }
    // writing file
    enum write_status w_status = to_bmp(output, &image_rotated.image);
    if(w_status) {
        bmp_print_write_status(w_status);
        goto destroy;
        return w_status;
    }
    image_destroy(&image_rotated.image);

destroy:
    image_destroy(&img);
    file_close(input);
    file_close(output);

    return 0;
}


static int open_files(const char *const ch_input, const char *const ch_output, FILE **const input, FILE **const output) {
    struct file_maybe fim = file_open(ch_input, "rb"); // fim = file_input_maybe
    struct file_maybe fom = file_open(ch_output, "wb");

    if (!fim.status && !fom.status) {
        *input = fim.file;
        *output = fom.file;
        return 0;
    } 
    if (!fim.status) file_close(fim.file);
    if (!fom.status) file_close(fom.file);
    return 1;
}

