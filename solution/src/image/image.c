#include "image.h"

#include <malloc.h>


static const struct image_optional image_none = {0};

struct image_optional image_some(const struct image img) {
    return (struct image_optional) {.is_valid = true, .image = img };
}


struct image_optional image_create(const uint64_t height, const uint64_t width){
    void* data = malloc(sizeof(struct pixel) * height * width);

    if (data == NULL) {
        return image_none;
    } else {
        return image_some((struct image) {.width = width, .height = height, .data = data});
    }
}

void image_destroy(struct image *img){
    if (img != NULL && img->data != NULL)
        free(img->data);
}
