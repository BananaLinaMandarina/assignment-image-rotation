#ifndef LAB_PIXEL_H
#define LAB_PIXEL_H


struct pixel {
    uint8_t b;
    uint8_t g;
    uint8_t r;
};

#endif //LAB_PIXEL_H
