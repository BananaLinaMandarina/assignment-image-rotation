#ifndef LAB_IMAGE_H
#define LAB_IMAGE_H

#include <inttypes.h>
#include <stdbool.h>

#include "pixel.h"

struct image {
    uint64_t width;
    uint64_t height;
    struct pixel *data;
};


struct image_optional {
    bool is_valid;
    struct image image;
};

static const struct image_optional image_none;

struct image_optional image_some(struct image img);


struct image_optional image_create(uint64_t height, uint64_t width);

void image_destroy(struct image *img);


#endif //LAB_IMAGE_H
