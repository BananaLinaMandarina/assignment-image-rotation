#include "bmpio.h"

#include <stdio.h>

#include "bmp_header.h"

// bmp constants
#define SIGNATURE 19778
#define RESERVED 0
#define HEADER_SIZE 40
#define PLANES 1
#define COMPRESSION 0
#define PIXEL_PER_M 2834
#define COLORS_USED 0
#define COLORS_IMPORTANT 0
#define BIT_COUNT 24


static uint8_t calculate_padding(size_t width);

static struct bmp_header prepare_bmp_header(const struct image *img) {
    struct bmp_header header = {0};
    uint8_t padding = calculate_padding(img->width);
    header.signature = SIGNATURE;
    header.image_size = img->height * (img->width * sizeof(struct pixel) + padding);
    header.filesize = header.image_size + sizeof(struct bmp_header);
    header.reserved = RESERVED;
    header.data_offset = sizeof(struct bmp_header);
    header.size = HEADER_SIZE;
    header.width = img->width;
    header.height = img->height;
    header.planes = PLANES;
    header.bit_count = BIT_COUNT;
    header.compression = COMPRESSION;
    header.x_pixels_per_m = PIXEL_PER_M;
    header.y_pixels_per_m = PIXEL_PER_M;
    header.colors_used = COLORS_USED;
    header.colors_important = COLORS_IMPORTANT;
    
    return header;
}

static enum read_status bmp_read_header(FILE *in, struct bmp_header *header) {
    if (fread(header, 1, sizeof(struct bmp_header), in) < sizeof(struct bmp_header))
        return READ_IO_ERR;

    return READ_OK;
}

enum read_status from_bmp(FILE *in, struct image *img) {
    if (!in || !img) { return READ_INVALID_ARGUMENTS; }

    struct bmp_header header;
    enum read_status stat = bmp_read_header(in, &header);
    if (stat != READ_OK) return stat;

    struct image_optional image_opt = image_create(header.height, header.width);
    if (image_opt.is_valid) {
        *img = image_opt.image;
    } else {
	    return READ_NOT_ENOUGH_MEMORY;
    }

    if (fseek(in, header.data_offset, SEEK_SET)) return READ_IO_ERR;
    uint8_t padding = calculate_padding(img->width);
    for (uint64_t y = 0; y < img->height; y++) {
        fread(img->data + y * (img->width), (size_t) (img->width) * sizeof(struct pixel), 1, in);
        if (ferror(in)) {
            image_destroy(img);
            img->data = NULL;
            return READ_IO_ERR;
        }
        if (padding) {
            if (fseek(in, padding, SEEK_CUR)) {
                image_destroy(img);
                img->data = NULL;
                return READ_IO_ERR;
            }
        }
    }
    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image *img) {
    if (!out || !img) { return WRITE_INVALID_ARGUMENTS; }

    struct bmp_header header = prepare_bmp_header(img);
    uint8_t padding = calculate_padding(img->width);

    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) return WRITE_ERROR;
    if (fseek(out, header.data_offset, SEEK_SET)) return WRITE_ERROR;

    uint8_t max_padding[4] = {0};
    // write pixels
    if (img->data){
        for (size_t y = 0; y < img->height; y++) {
            if (fwrite(img->data + y * img->width, img->width * sizeof(struct pixel), 1, out) != 1) return WRITE_ERROR;
            if (padding) {
                if (fwrite(max_padding, padding, 1, out) != 1) return WRITE_ERROR;
            }
        }
    }
    return WRITE_OK;
}

static uint8_t calculate_padding(const uint64_t width) {
    return (4 - (width * (BIT_COUNT / 8) % 4)) % 4;
}

#undef SIGNATURE
#undef RESERVED
#undef HEADER_SIZE
#undef PLANES
#undef COMPRESSION
#undef PIXEL_PER_M
#undef COLORS_USED
#undef COLORS_IMPORTANT
#undef DOUBLE_WORD
#undef BIT_COUNT
