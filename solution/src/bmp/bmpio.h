#ifndef LAB_BMPIO_H
#define LAB_BMPIO_H

#include <stdio.h>

#include "../image/image.h"
#include "bmp_status.h"

enum read_status from_bmp(FILE *in, struct image *img);
enum write_status to_bmp(FILE *out, struct image *img);

#endif //LAB_BMPIO_H
