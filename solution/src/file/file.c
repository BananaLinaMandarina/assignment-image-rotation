#include "file.h"

struct file_maybe file_open(const char * const name, const char *mode) {
    FILE* file = fopen(name, mode);

    if (file) {
        return (struct file_maybe) {.file = file, .status = OK };
    } else {
    	return (struct file_maybe) {.file = NULL, .status = FILE_ERROR};
    }
}

bool file_close(FILE* const file) {
    if (!file) return true;

    return !fclose(file);
}
