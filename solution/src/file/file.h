#ifndef LAB_FILE_H
#define LAB_FILE_H

#include <stdbool.h>
#include <stdio.h>

#include "file_status.h"


struct file_maybe {
    enum file_status status;
    FILE *file;
};

struct file_maybe file_open(const char * const name, const char *mode);

bool file_close(FILE* const file);


#endif //LAB_FILE_H
