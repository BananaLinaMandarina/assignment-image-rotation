#ifndef LAB_ROTATE_H
#define LAB_ROTATE_H

#include "../image/image.h"


struct image_optional rotate(struct image source);

#endif //LAB_ROTATE_H
