#include "rotate.h"


struct image_optional rotate(const struct image source){
    uint64_t width = source.width;
    uint64_t height = source.height;
    struct pixel *data = source.data;

    // width and height are reversed
    struct image_optional img_opt = image_create(height, width);
    if (!img_opt.is_valid) {
        return image_none;
    } else {
        struct pixel *new_data = img_opt.image.data;

        for (uint64_t y = 0; y < height; y++) {
            for (uint64_t x = 0; x < width; x++) {
                new_data[x * height + (height - 1 - y)] = data[y * width + x];
            }
        }
        return image_some((struct image) {
                .width = img_opt.image.height,
                .height = img_opt.image.width,
                .data = new_data
        });
    }
}
