#ifndef LAB_BMP_ERROR_H
#define LAB_BMP_ERROR_H

#include "../bmp/bmp_status.h"

void bmp_print_read_status(enum read_status status);

void bmp_print_write_status(enum write_status status);

#endif //LAB_BMP_ERROR_H
