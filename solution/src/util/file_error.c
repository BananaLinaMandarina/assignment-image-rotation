#include "file_error.h"

#include <stdio.h>

void file_print_status(enum file_status status) {

    static const char* file_status[] = {
            [OK] = "FILE. Everything is ok.",
            [FILE_ERROR] = "FILE. Error."
    };

    if (status) {
        fprintf(stderr, "%s\n", file_status[status]);
    }
    else {
        fprintf(stdout, "%s\n", file_status[status]);
    }
}
