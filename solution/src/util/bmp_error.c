#include "bmp_error.h"

#include <stdio.h>

void bmp_print_read_status(enum read_status status) {
    static char* read_status[] = {
            [READ_OK] = "BMP READ. Everything is ok",
            [READ_INVALID_SIGNATURE] = "BMP READ. Invalid signature",
            [READ_INVALID_BITS] = "BMP READ. Invalid bits",
            [READ_INVALID_HEADER] = "BMP READ. Invalid header",
            [READ_INVALID_ARGUMENTS] = "BMP READ. Invalid arguments given",
            [READ_NOT_ENOUGH_MEMORY] = "BMP READ. Not enough memory to alloc image"
    };

    if (status) {
        fprintf(stderr, "%s\n", read_status[status]);
    } else {
        fprintf(stdout, "%s\n", read_status[status]);
    }
}

void bmp_print_write_status(enum write_status status) {
    static char* write_status[] = {
            [WRITE_OK] = "BMP WRITE. Everything is ok",
            [WRITE_ERROR] = "BMP WRITE. Some error occupied while wring",
            [WRITE_INVALID_ARGUMENTS] = "BMP_WRITE. Invalid arguments given"
    };

    if (status) {
        fprintf(stderr, "%s\n", write_status[status]);
    } else {
        fprintf(stdout, "%s\n", write_status[status]);
    }
}

